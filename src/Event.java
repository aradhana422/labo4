
public class Event {

		//method that takes and returns nothing.
		void method1() {
			System.out.println("Method that takes and receives nothing.");
		}
	//method that takes parameter but returns nothing.
		void method2(int a) {
			System.out.println("The value of integer \"a\" is: " + a);
		}
	//method that takes parameter and returns value.
		int method3(int b, int c) {
			int sum = b + c;
			return sum;

		
		

	}

}
