
public class CalculatorClient {

	public static void main(String[] args) {
		
		Calculator c = new Calculator();
		
		
		System.out.println("The sum is: " + c.add(2,4));
		System.out.println("The difference is: " + c.subtract(4.6,5));
		System.out.println("The product is: " + c.multiply(2,5));
		System.out.println("The divison is: " + c.divide(8,3));
		System.out.println("The string Value is: " + c.findStringLength("How you doing?"));

	}

}
